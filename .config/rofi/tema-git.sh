#!/bin/env bash

# intento modificar el tema de bspwm usando las ramas de git
# tengo 2 ramas branches funcionales
# bspwm-feh      -      es con feh y la configuración básica de polybar
# bspwm-color    .      está basada en la rama polybar-new con polybar en color pero ahora con feh

# ramas anteriores
# polybar-old    -      esta rama se fusionó con master la primera vez que probé gitlab
# polybar-new    -      sigue con nitrogen y varios errores - mejor no usar
# master         -      ahora tiene la configuración básica de bspwm y polybar pero con errores


bspwmfeh="󱆃   Tema Polybar Básico"
bspwmcolor="   Tema Polybar Colorido"

# menu sencillo con rofi

selected_option=$(echo "$bspwmfeh
$bspwmcolor" | rofi -dmenu\
                -i\
                -p "Temas BSPWM"\
                -width "20"\
                -lines 4\
                -line-margin 3\
                -line-padding 10\
                -theme $HOME/.config/rofi/themes/nord\
                -scrollbar-with "0" )

#  cambio el branch dependiendo de la selección

#  debo estar en el directorio .config
cd $HOME/.config

if [ "$selected_option" == "$bspwmfeh" ]; then
     git checkout bspwm-feh
     sleep 2
     bspc wm -r
     sleep 2
     bspc wm -r

elif [ "$selected_option" == "$bspwmcolor" ]; then
     git checkout bspwm-color
     sleep 2
     bspc wm -r
     sleep 2
     bspc wm -r

else
     echo "Cancelado"
fi


#  ya sé que podría usar un case y una variable
#  pero yo empecé con BASIC
#  así que mi lógica de programación
#  quedó atrofiada de por vida :-)
