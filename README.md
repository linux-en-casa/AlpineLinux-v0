# Alpine Linux - XFCE - BSPWM - Qtile

## Advertencia:
Primera versión de la configuracíón de Alpine Linux en el Portátil Compaq CQ45.

Inicialmente instalé Alpine Linux versión 3.19; luego instalé el Entorno de Escritorio XFCE.
Configuré el teclado y el idioma en Español.  También se configuró el sonido con Pipewire.

El siguiente paso fue instalar el gestor de ventanas BSPWM.  Para su configuración empecé con los archivos que tengo en el repositorio de gitlab:
[gitlab - bspwm](https://gitlab.com/linux-en-casa/Bspwm) .

Estoy usando la rama bspwm-feh.

En Alpine no encontré todas las apps que utilizo en Debian GNU/Linux 12, por ejemplo no encontré nitrogen.  Pero al final ajusté los archivos de configuración para que todo funcionara relativamente bien.

El problema es que al iniciar bspwm no tengo sonido (debe faltar arrancar pipewire).

La solución que he encontrado es usar XFCE pero con el gestor de ventanas BSPWM.

Para la instalación de Qtile fue necesario cambiarme a la rama Edge activando main, community y testing.
De nuevo en Qtile no tengo sonido así que también integré XFCE con Qtile...  Anque el cambio de XFCE con BSPWM  a XFCE con Qtile es demasiado rudimentario (activando y desactivando desde Sesión e Inicio de XFCE)


A partir de aquí empezaré a realizar los Videos y Directos de Instalación y Configuración del canal:
[YouTube, Linux en Casa](https://www.youtube.com/LinuxenCasa) .
 
También en Twitch:
[Twitch, Linux en Casa](https://www.twitch.tv/linuxencasa) .

La idea es terminar con una configuración como la muestro en este video:
[Directo en YouTube](https://www.youtube.com/watch?v=FTVO-Cvj5Wc)



**GNU General Public License v3.0**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


