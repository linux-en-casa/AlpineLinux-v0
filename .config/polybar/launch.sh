#!/usr/bin/env bash

# Terminate already running bar instances
# If all your bars have ipc enabled, you can use 

# esta línea viene por defecto
# pero tal vez por eso cuando reinicio bspwm
# la barra externa de polybar queda en primer plano 
# y no deja que las ventanas se muestren en full screen
# uso killall y pruebo 

# polybar-msg cmd quit

# Otherwise you can use the nuclear option:
killall -q polybar
#  no funciona - da igual con una y otra opcion
#  hay que ver alguna opcion en el config.ini de las barras
sleep 2

# averiguar el numero de monitores conectados
SCREEN=$(xrandr | grep " connected " | wc -l)
# La variable SCREEN valdrá 1, 2, 3, etc dependiendo lo que detecte xrandr

if [[ $SCREEN -eq 1 ]]; then
echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log

pantalla=$(xrandr | grep " connected " | awk '{print $1}' | sed -n 1p)
MONITOR=$pantalla polybar barra-unica 2>&1 | tee -a /tmp/polybar2.log & disown
# polybar barra-unica 2>&1 | tee -a /tmp/polybar1.log & disown
echo "Bars launched..."
fi

if [[ $SCREEN -eq 2 ]]; then
# Lanzar las 2 barras 
echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log

pantalla1=$(xrandr | grep " connected " | awk '{print $1}' | sed -n 1p)
MONITOR1=$pantalla1 polybar barra-interna 2>&1 | tee -a /tmp/polybar2.log & disown
# polybar barra-interna 2>&1 | tee -a /tmp/polybar1.log & disown
sleep 1

pantalla2=$(xrandr | grep " connected " | awk '{print $1}' | sed -n 2p)
MONITOR2=$pantalla2 polybar barra-externa 2>&1 | tee -a /tmp/polybar2.log & disown
echo "Bars launched..."
fi


