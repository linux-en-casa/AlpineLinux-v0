#!/bin/ash

# en alpine no está nitrogen - intento con feh
# nitrogen --restore &

# copio los comandos de bspwm
# feh para el fondo de la pantalla
feh --bg-center "${HOME}/.config/qtile/wallpapers/wp1.png" "${HOME}/.config/qtile/wallpapers/wp2.png" &

# picom &


#  iniciar pipewire
/usr/libexec/pipewire-launcher &

#  teclado en espaniol
setxkbmap -model pc105 -layout es &

