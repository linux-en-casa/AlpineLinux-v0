#!/bin/env bash

ids=($(bspc query -N -n .hidden.window))
(( "${#ids[@]}" )) || exit
for wid in "${ids[@]}"; do
#    cualquiera de estas 2 da error
#     title="$(xtitle "$wid")"
#    title="$( "$wid")"
    echo "valor de wid"
    echo $wid
    options+="${title:-"$(bspc query -T -n "$wid" | jq -r '
        .client | "\(.instanceName):\(.className)"
    ')"}"$'\n'
done

id_index="$(<<< "$options" rofi -dmenu -i -format i -p "Show" -theme $HOME/.config/rofi/themes/rounded-blue-dark)"
bspc node "${ids[${id_index}]}" -g hidden=off -f; bspc node "${ids[${id_index}]}" -g sticky=off -f


# este script muestra un menu sencillo con las ventanas ocultas
# las ventanas se ocultan con Super + Alt + o
# y este script se muestra con Super + Alt + u
# ver atajos de teclado de sxhkd
