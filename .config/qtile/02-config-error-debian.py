# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

############################################################
#  LEEME PRIMERO
#  Empiezo el 1 de Dic de 2023 y estoy ajustando la configuración
#  con algunas o muchas líneas de Arco Linux

#############################################################
#  librerias - modulos funciones y comandos a importar

from libqtile import bar, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
# from libqtile.utils import guess_terminal
# no necesito  guess_terminal pues configuro un solo terminal
# abajo en las variables 

# para que funcione nitrogen - ver al final - sección hook
import os
import subprocess
from libqtile import hook


##############################################################

#  Variables - configurar o modificar al gusto
#  hacen parte de los atajos de teclado de la siguiente sección
####
# La tecla predeterminada para los atajos es la del logo de Windows
# o tecla Super ...   Alt sería mod1 
mod = "mod4"

# terminal = guess_terminal()
terminal = "alacritty"

# dmenu en la parte superior con 6 líneas
menu = "dmenu_run -l 6 -i -p 'dmenu, Abrir una App: '"

# lanzar rofi para abrir aplicaciones
rofi = "rofi -show-icons -show drun"

# Capturar la Pantalla - screenshooter
capturar = "xfce4-screenshooter"

###############################################################
# Atajos de teclado
####

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    # Agrego los mismos atajos con las flechas - tomado de arco linux - qtile
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Up", lazy.layout.up()),


    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Agrego estos 4 atajos de arriba con las flechas - también de Arco Linux
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),



    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Nuevamente esto 4 atajos con las flechas -  también de Arco Linu
    # Aunque Arco Linux crea demasiados atajos - yo solo replico lo que viene por defecto
    Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),


    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    ####
    # He agregado estos --  ver las variables -  arriba
    ####
    # lanzar dmenu con Super + Alt + d ...   Alt es mod1
    Key([mod, "mod1"], "d", lazy.spawn(menu), desc="Launch dmenu"),

    # lanzar rofi con Super + Alt + r ...   Alt es mod1
    Key([mod, "mod1"], "r", lazy.spawn(rofi), desc="Lanzar Rofi"),

    # Capturar la pantalla - screenshooter
    Key([], "Print", lazy.spawn(capturar), desc="Capturar Pantalla"),

    # teclas de volumen
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle"), desc="Silencio - Mute"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 sset Master 5- unmute"), desc="Bajar Volumen - Volume down"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 sset Master 5+ unmute"), desc="Bajar Volumen - Volume down"),

    # Hasta aquí lo que he agregado
    ####

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
]

# Fin de atajos de teclado, más abajo está lo que se hace con el mouse y el manejo de los Escritorios
################################################################


# Add key bindings to switch VTs in Wayland.
# We can't check qtile.core.name in default config as it is loaded before qtile is started
# We therefore defer the check until the key binding is run by using .when(func=...)
for vt in range(1, 8):
    keys.append(
        Key(
            ["control", "mod1"],
            f"f{vt}",
            lazy.core.change_vt(vt).when(func=lambda: qtile.core.name == "wayland"),
            desc=f"Switch to VT{vt}",
        )
    )

##################################################################
# En qtile groups son los Escritorios o Espacios de Trabajo
####

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + group number = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + group number = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + group number = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )



########################################################################
# adicional - configurar un tema para algunos layouts - más abajo
layout_theme = {
        "border_width": 5,
        "margin": 10,
        "border_focus": "FF0000",
        "border_normal": "CCCCCC",
        "border_on_single": True
        }
########################################################################

layouts = [
    layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    layout.Stack(num_stacks=2),
    # layout.Bsp(),
    layout.Bsp(**layout_theme),
    layout.Matrix(),
    layout.MonadTall(),
    layout.MonadWide(),
    layout.RatioTile(),
    layout.Tile(),
    layout.TreeTab(),
    layout.VerticalTile(),
    layout.Zoomy(),
]

widget_defaults = dict(
    font="sans bold",
    fontsize=16,
    padding=3,
)
extension_defaults = widget_defaults.copy()

#################################################################
#  configuración de las "Screens"
#  uso fake screen para crear 3 "screens"
# La screen 1 sería la pantalla interna - tal cual
# screen 2 y screen 3 están en la pantalla externa
# aprovecho que la pantalla externa es 16:10
# dejo la screen 2 de 16:9
# y abajo screen 3 de 16:1 que me sirve para poner screenkey


fake_screens = [

#  screen1 en pantalla interna 1366 x 768   en 0,0

    Screen(
        top=bar.Bar(
            [
                widget.TextBox("Debian GNU/Linux ", foreground="#d75f5f"),
                widget.CurrentLayout(),
                widget.GroupBox(),
                # widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                widget.Systray(),

                # volumen - se debe instalar alsa-utils
                widget.Volume(),

                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                widget.QuickExit(),
            ],
            36
        ),
        x=0,
        y=0,
        width=1366,
        height=768

            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
         # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),

######################################################
#  fin de pantalla interna
#####################################################

#  screen 2 en pantalla externa pero a 16:9
#  x en 1366  y = 0
#  ancho  1440  y altura 810 


    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                # widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                # widget.Systray(),

                # volumen - se debe instalar alsa-utils
                widget.TextBox("Volumen -->", foreground="#d75f5f"),
                widget.Volume(),

                # widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                # widget.QuickExit(),
            ],
            36
        ),
        x=1366,
        y=0,
        width=1440,
        height=810

            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
         # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),


#####################################################
#####   fin de screen 2


#  queda debajo una screen 3
#  con solo 90 de altura
#  la utilizaré para el screenkey

    Screen(
        bottom=bar.Bar(
            [
                # widget.CurrentLayout(),
                # widget.GroupBox(),
                widget.TextBox("(-: Like - Subscribe :-)", foreground="#ff0000"),
                # pruebo el widget.Sep - una barra para separar los widgets
                # visto en Arco Linu
                widget.Sep(padding = 20),
                widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),

                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                # widget.TextBox("default config", name="default"),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                # widget.Systray(),

                widget.Sep(padding = 20),

                # volumen - se debe instalar alsa-utils
                widget.TextBox("Volumen -->", foreground="#0000ee"),
                widget.Volume(),

                widget.Sep(padding = 20),
                widget.TextBox("Fecha --> ", foreground="#00ff00"),
                widget.Clock(format="%Y-%m-%d %a %I:%M %p"),

                widget.Sep(padding = 20),
                widget.TextBox("Batería --> ", foreground="#00ff00"),
                widget.Battery(),
                widget.Sep(padding = 20),
                widget.TextBox("¡Ojo! Logout en 4 segs --> ", foreground="#ff0000"),
                widget.QuickExit(),
            ],
            36
        ),
        x=1366,
        y=810,
        width=1440,
        height=90

            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
         # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
        # By default we handle these events delayed to already improve performance, however your system might still be struggling
        # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
        # x11_drag_polling_rate = 60,
    ),


]

####
# hasta aquí lo de las fake-screens
###################################################################
# Sigue el manejo de las ventanas flotantes con el mouse


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"



###########################################################################
#  Adicionales
###########################################################################
#  HOOK STARTUP - Ver comienzo del archivo
@hook.subscribe.startup_once
def autostart():
    if qtile.core.name == "x11":
        autostartscript = "~/.config/qtile/autostart_x11.sh"
    elif qtile.core.name == "wayland":
        autostartscript = "~/.config/qtile/autostart_wayland.sh"

    home = os.path.expanduser(autostartscript)
    subprocess.Popen([home])

###########################################################################

